var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
 
// var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral'];
 
 
// var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;';
 
var recognition  = new SpeechRecognition();
var speechrecognitionlist = new SpeechGrammarList();
 
// speechrecognitionlist.addFromString(grammar, 1);
 
// recognition.grammars = speechrecognitionlist;
recognition.lang = "nl-NL";
// recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;
 
 
var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');
var hints = document.querySelector('.hints');
 
var outputbig = document.querySelector(".outputbig");
var ellieoutput = document.querySelector(".output-ellie");
 
var image = document.querySelector(".image");
 
var trigger = false;
var startvalue = false;
var counter = 0;



var teller = 0;
var alGetrggrd = false;


 
recognition.start();
 
// document.onclick = function() {
//     recognition.start();
//     console.log('Ready to receive a color command.');
// };
 
 
// document.getElementById("hint").innerHTML = "Try one of the following commands: " + colors.toString();
 
var globalresult;
 
recognition.onresult = function(event) {
    var last = event.results.length - 1;
    var result = event.results[last][0].transcript;
 
    diagnostic.textContent = "I recieved: " + result + " with a confidence of: " + event.results[0][0].confidence;
    outputbig.textContent = result;
    console.log("Recieved something!");
 
    globalresult = result.toLowerCase();
 
 
    if(result.toLowerCase() === "start" && trigger === false){
        trigger = true;
        startvalue = true;
        counter = 0;
 
    }
    else if(result.toLowerCase() === "stop"){
        startvalue = false;
        trigger = false;
    }
    else{
        startvalue = false;
    }
 
    if(startvalue === true){
        saythings("Hallo");
        checkIfResponse(result.toLowerCase());
    }
 
 
    if(result.toLowerCase() === "grap"){
        randomgrap();
    }
 
 
 
 
    // switch(result.toLowerCase()){
    //     case "hallo":
    //         var randomnum = Math.floor((Math.random() * 2));
    //         var myArray = ["Hello!", "Yes?", "Speaking", "What's up!", "Hello Earth"];
    //         var myArrayNL = ["Hallo", "Hallo daar", "Goededag"];
    //         // var myArray = ["Please stop talking", "Just stop, I am not in the mood"];
    //         // saythings(myArray[randomnum]);
    //         saythings(myArrayNL[randomnum]);
    //         ellieoutput.innerHTML = myArrayNL[randomnum];
    //
    //         break;
    //     case "hallo daar":
    //         console.log("Hello there recieved!");
    //         image.src = "https://i.imgur.com/VDRXqTn.jpg";
    //         break;
    //     case "clear":
    //         image.src = "";
    //         break;
    //     case "goedemorgen":
    //         // var audio = new Audio('audio/cant.wav');
    //         // audio.play();
    //
    //         saythings("goedemorgen");
    //         break;
    //     case "dave":
    //         saythings("i'm sorry dave i'm afraid i can't do that");
    //         break;
    //     case "koffie":
    //         // saythings("Het loopt in de wei, en helpt tegen menstuatie? --------------------------------------------------------------------------------------------- Een tampony");
    //         saythings("Ik kan niet meer ! ! ! ! ! !  ik ben gewoon op");
    //         break;
    //     case "pot":
    //         saythings("Kom eens van die pot af ... jongen je zit er al een half uur op");
    //     break;
    //
    // }
 
 
};
 
// if(trigger === true){
//     console.log("hier ben ik");
//
// }
 
function checkIfResponse(){
    if(counter < 10 ) {
        setTimeout(function () {
            counter++;
            console.log(counter);
            if(startvalue === true){
                console.log("No response");
            }
            else{
                console.log("Response " + globalresult);
                counter = 9999;
                if(trigger == true){
                    response(globalresult); 
                }
                return 0;
            }
            checkIfResponse();
        }, 1000);
    }
    else if(counter === 10){
        noresponse();
    }
    else{
    }
    return 0;
}
 
 
 
function response(globalresult){
    var randnum = Math.floor(Math.random() * 10);
    randnum = 1;
    switch(randnum) {
        case 0:
            var str = globalresult;
            var amountwords = str.split(" ").length;
            var word = str.split(" ")[Math.floor(Math.random() * amountwords)];
 
            var keyword = word;
            $.getJSON('https://nl.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles='+ keyword +'&redirects=true&explaintext&exintro&origin=*',
                function(data) {
                    var page = data.query.pages;
                    // console.log(page);
                    pageId = Object.keys(data.query.pages)[0];
                    var content = page[pageId].extract;
                    if(content != null){
                        console.log(content);
                        var sentence = content.split(".")[0];
                        console.log("Sentence: " + sentence);
                        saythings(word + sentence);
 
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 10000);
                    }
                    else{
                        randomgrap();
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 5000);
                    }
 
                });
            console.log("Result in responsefunction: " + word);
            break;
        case 1:{
            saythings("Hallo");
            saythings("mijn");
            saythings("naam");
            saythings("is");
            saythings("jemoeder");
        }
    }
 
 
 
}
 
 
function noresponse(){
    var randnum = Math.floor(Math.random() * 10);
    counter = 0;
    switch(randnum){
        case 0:
            saythings("Waarom negeer je mij?!");
            checkIfResponse();
            break;
        case 1:
            saythings("je maakt me aan het huilen");
            checkIfResponse();
            break;
        case 2:
            randomgrap();
            setTimeout(function(){
                checkIfResponse();
            }, 5000);
 
            break;
        default:
            saythings("standaard antwoord");
            if(!synth.speaking){
            }
            else{
                checkIfResponse();
            }
            break;
    }
 
    return 0;
}
 
 
function ifresponse(){
 
}
 
function randomgrap(){
    var actual_JSON;
    loadJSON(function(response) {
 
        actual_JSON = JSON.parse(response);
        var randnum = Math.floor(Math.random() * actual_JSON.length + 1);
 
        if(!synth.speaking){
            saythings(actual_JSON[randnum]["buildup"]);
            setTimeout(function(){
                saythings(actual_JSON[randnum]["punchline"]);
            }, 1000);
        }
 
        // console.log(actual_JSON[randnum]["buildup"]);
        // console.log(actual_JSON[randnum]["punchline"]);
    });
}
 
// grappen laden
function loadJSON(callback) {
 
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'grappen.json', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}
 
 
 
recognition.onend = function() {
    recognition.start();
};
 
var synth = window.speechSynthesis;
var voices = [];
 
function populateVoiceList() {
    voices = synth.getVoices();
    // console.log(synth.getVoices())
}
 
 
voices = synth.getVoices();
 
if (speechSynthesis.onvoiceschanged !== undefined) {
    speechSynthesis.onvoiceschanged = populateVoiceList;
 
}
 
 
 
function saythings(whattosay){
    var utterThis = new SpeechSynthesisUtterance(whattosay);
    // var selectedOption = "Google UK English Female";
    var selectedOption = "";
    for (i = 0; i < voices.length; i++) {
        if (voices[i].name === selectedOption) {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1;
    utterThis.rate = 1;
    synth.speak(utterThis);
 
    // console.log(utterThis.voice);
}
 
// console.log(synth.getVoices());






var socket = io();
socket.on('sensor', function(data) {   
                
    data = data/20;
    if(data > 1){
      data = 1;
    }
    
    console.log(data);
 
    document.getElementById("mijndivje").innerHTML = data + "  " + teller;

    if (data < 1 && alGetrggrd == false && teller > 100) {
        startvalue = true;
        saythings("Hallo");
        checkIfResponse();
        counter = 0;
        teller = 0;
        veranderAchtergrond("red");
        alGetrggrd = true;
    } else if (data < 1 && alGetrggrd == true && teller > 250) {
        startvalue = false;
        alGetrggrd = false;
        trigger = false;
        teller = 0;
    } else {
         veranderAchtergrond("blue");
         teller++;
         
    }
});
    
 function veranderAchtergrond(kleur) {
   document.body.style.backgroundColor = kleur;
 }
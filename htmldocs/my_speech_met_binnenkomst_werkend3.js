var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
 
// var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral'];
 
 
// var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;';
 
var recognition  = new SpeechRecognition();
var speechrecognitionlist = new SpeechGrammarList();
 
// speechrecognitionlist.addFromString(grammar, 1);
 
// recognition.grammars = speechrecognitionlist;
recognition.lang = "nl-NL";
// recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;
 
 
var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');
var hints = document.querySelector('.hints');
 
var outputbig = document.querySelector(".outputbig");
var ellieoutput = document.querySelector(".output-ellie");
 
var image = document.querySelector(".image");
 
var trigger = false;
var startvalue = false;
var counter = 0;
var utterthis;


var teller = 0;
var alGetrggrd = false;

 
recognition.start();
 
var globalresult;
 
recognition.onresult = function(event) {
    var last = event.results.length - 1;
    var result = event.results[last][0].transcript;
 
    diagnostic.textContent = "I recieved: " + result + " with a confidence of: " + event.results[0][0].confidence;
    outputbig.textContent = result;
    console.log("Recieved something!");
 
    globalresult = result.toLowerCase();
 
 
    if(result.toLowerCase() === "start" && trigger === false){
        trigger = true;
        startvalue = true;
        counter = 0;
 
    }
    else if(result.toLowerCase() === "stop"){
        startvalue = false;
        trigger = false;
    }
    else{
        startvalue = false;
    }
 
    if(startvalue === true){
        saythings("Hallo");
        checkIfResponse(result.toLowerCase());
    }
 
 
    if(result.toLowerCase() === "grap"){
        randomgrap();
    }
 
};
 
function checkIfResponse(){
    if(counter < 10 ) {
        setTimeout(function () {
            counter++;
            console.log(counter);
            if(startvalue === true){
                console.log("No response");
            }
            else{
                console.log("Response " + globalresult);
                counter = 9999;
                console.log("Hier zijn we");
                if(trigger == true){
                    console.log("Hier zijn we");
                    response(globalresult);
                }
                return 0;
            }
            checkIfResponse();
        }, 1000);
    }
    else if(counter === 10){
        noresponse();
    }
    else{
    }
    return 0;
}
 
 
 
function response(globalresult){
    var randnum = Math.floor(Math.random() * 4);
    // randnum = 3;
 
    switch(randnum) {
 
        // wikipedia pagina
        case 0:
            var str = globalresult;
            var amountwords = str.split(" ").length;
            var word = str.split(" ")[Math.floor(Math.random() * amountwords)];
            var keyword = word;
            $.getJSON('https://nl.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles='+ keyword +'&redirects=true&explaintext&exintro&origin=*',
                function(data) {
                    var page = data.query.pages;
                    // console.log(page);
                    pageId = Object.keys(data.query.pages)[0];
                    var content = page[pageId].extract;
                    if(content != null){
                        console.log(content);
                        var sentence = content.split(".")[0];
                        console.log("Sentence: " + sentence);
                        saythings(word + sentence);
 
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 10000);
                    }
                    else{
                        randomgrap();
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 5000);
                    }
 
                });
            console.log("Result in responsefunction: " + word);
            break;
 
            // belachelijk maken
        case 1:
            var newrandnum = Math.floor(Math.random() * 2);
            switch(newrandnum){
                // Je praat tegen een WC
                case 0:
                    saythings("Praat je nou terug tegen een WC?");

                    counter = 0;
                    startvalue = true;
                    setTimeout(function(){
                        checkIfResponse();
                    }, 5000);
                    break;
                    //
                case 1:
                    saythings("Je praat nu tegen een levensloos object, hoe kansloos is dat haha");

                    counter = 0;
                    startvalue = true;
                    setTimeout(function(){
                        checkIfResponse();
                    }, 5000);
                    break;
                default:
                    saythings("Wat een leuk shirt heb je aan");
                    saythings("Oh wacht, toch niet");

                    counter = 0;
                    startvalue = true;
                    setTimeout(function(){
                        checkIfResponse();
                    }, 5000);
            }
            break;
 
        // Levensverhaal
        case 2:
            saythings("Dit is mijn levensverhaal");
 
            saythings("Jester werkte als nar bij het koninklijk hof. Jester was niet alleen goed in het maken van grappen. Jester was slim genoeg om zaken te doen voor de koning. Jester gebruikte de koning als persoon in zijn moppen.");
            saythings("Op een dag maakte Jester zoals gewoonlijk grapjes over de koning, maar de koning voelde zich beledigd en riep: Bewakers, grijp Jester! Zet hem in de gevangenis, over drie dagen hangen we Jester op.");
            saythings("Drie dagen later werd Jester voor de rechter gedaagd. De koning vroeg aan Jester: Hoe wil je sterven?");
            saythings("Jester zegt hierop: Majesteit, ik kies ervoor om op hoge leeftijd te sterven. ");
            saythings("Ha….. ha….. ha……. ");
            saythings("Jester begon te lachen en de koning lachte met hem mee. De koning vergaf Jester. Jester bewees hiermee dat humor sterker is dan de sterkste man op aarde. ");
            saythings("Tegenwoordig neemt Jester mensen in de maling op de wc.");

            counter = 0;
            startvalue = true;
            setTimeout(function(){
                checkIfResponse();
            }, 10000);
            break;
 
            // napraten
        case 3:
            saythings(globalresult);

            counter = 0;
            startvalue = true;
            setTimeout(function(){
                checkIfResponse();
            }, 5000);
            break;
    }
}
 
 
function noresponse(){
    var randnum = Math.floor(Math.random() * 4);
    counter = 0;
    switch(randnum){
 
        // Waarom negeer je mij?
        case 0:
            saythings("Waarom negeer je mij?!");
            checkIfResponse();
            break;
 
            // Huilen
        case 1:
            saythings("je maakt me aan het huilen");
            setTimeout(function(){ var audio = new Audio('audio/huilen.mpeg'); 
            audio.play();
            }, 2800);
            
 
            setTimeout(function(){
                checkIfResponse();
            }, 5000);
            break;
 
            // Grap
        case 2:
            randomgrap();
            setTimeout(function(){
                checkIfResponse();
            }, 5000);
            break;
 
 
            //standaard antwoord
        case 3:
            saythings("Als er toch niemand is, zet ik het licht wel uit.");

            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 2800);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow";
            // var audio = new Audio('audio_file.mp3');
            // audio.play();
            }, 8000);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 8230);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow";
            // var audio = new Audio('audio_file.mp3');
            // audio.play();
            }, 8700);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 8880);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow"; }, 9000);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 9180);
            setTimeout(function(){
                checkIfResponse();
            }, 20000);
            break;
        default:
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
 
 
            setTimeout(function(){
                checkIfResponse();
            }, 5000);
    }
 
    return 0;
}
 
 
function endjokesound(){
    var randnum = Math.floor(Math.random() * 3);
    var audio;
    switch(randnum){
        case 0:
            audio = new Audio('audio/krekel.mpeg');
            break;
        case 1:
            audio = new Audio('audio/badumtss.mp3');
            break;
        case 2:
            audio = new Audio('audio/lachen.mpeg');
            break;
    }
    audio.play();
}
 
 
 
function randomgrap(){
    var actual_JSON;
    loadJSON(function(response) {
 
        actual_JSON = JSON.parse(response);
        var randnum = Math.floor(Math.random() * actual_JSON.length + 1);
 
        if(!synth.speaking){
            saythings(actual_JSON[randnum]["buildup"]);
            setTimeout(function(){
                saythingswithend(actual_JSON[randnum]["punchline"], endjokesound);
            }, 1000);
        }
        // console.log(actual_JSON[randnum]["buildup"]);
        // console.log(actual_JSON[randnum]["punchline"]);
    });
}
 
// grappen laden
function loadJSON(callback) {
 
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'grappen.json', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}
 
 
 
recognition.onend = function() {
    recognition.start();
};
 
var synth = window.speechSynthesis;
var voices = [];
 
function populateVoiceList() {
    voices = synth.getVoices();
    // console.log(synth.getVoices())
}
 
 
voices = synth.getVoices();
 
if (speechSynthesis.onvoiceschanged !== undefined) {
    speechSynthesis.onvoiceschanged = populateVoiceList;
 
}
 
 
 
function saythings(whattosay){
 
    utterThis = new SpeechSynthesisUtterance(whattosay);
    // var selectedOption = "Google UK English Female";
    var selectedOption = "";
    for (i = 0; i < voices.length; i++) {
        if (voices[i].name === selectedOption) {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1;
    utterThis.rate = 1;
    synth.speak(utterThis);
 
    // console.log(utterThis.voice);
}
 
function saythingswithend(whattosay, thefunction){
 
    utterThis = new SpeechSynthesisUtterance(whattosay);
    // var selectedOption = "Google UK English Female";
    var selectedOption = "";
    for (i = 0; i < voices.length; i++) {
        if (voices[i].name === selectedOption) {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1;
    utterThis.rate = 1;
    synth.speak(utterThis);
 
    utterThis.onend = function(event) {
        thefunction()
    }
 
 
    // console.log(utterThis.voice);
}
 
// console.log(synth.getVoices());





var socket = io();
socket.on('sensor', function(data) {   
                
    data = data/20;
    if(data > 1){
      data = 1;
    }
    
    console.log(data);
 
    document.getElementById("mijndivje").innerHTML = data + "  " + teller;

    if (data < 1 && alGetrggrd == false && teller > 100) {
        startvalue = true;
        saythings("Hallo");
        checkIfResponse();
        counter = 0;
        teller = 0;
        trigger = true;
        alGetrggrd = true;
    } else if (data < 1 && alGetrggrd == true && teller > 250) {
        startvalue = false;
        alGetrggrd = false;
        trigger = false;
        teller = 0;
    } else {
         teller++;
         
    }
});
    
 function veranderAchtergrond(kleur) {
   document.body.style.backgroundColor = kleur;
 }
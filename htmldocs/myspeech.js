var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

// var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral'];
// var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;';

var recognition  = new SpeechRecognition();
var speechrecognitionlist = new SpeechGrammarList();

// speechrecognitionlist.addFromString(grammar, 1);
// recognition.grammars = speechrecognitionlist;

recognition.lang = "nl-NL";
// recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;


var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');
var hints = document.querySelector('.hints');

var outputbig = document.querySelector(".outputbig");
var ellieoutput = document.querySelector(".output-ellie");

var image = document.querySelector(".image");

var trigger = false;
var startvalue = false;
var counter = 0;
var utterthis;


// database variables
var user_key;
var scenario;
var user_response;
var walkedinorout;
var lastresponse = "";

var teller = 0;
var alGetrggrd = false;

recognition.start();

var globalresult;

// When the recognition engine gets a result. -- Sander
recognition.onresult = function(event) {
    var last = event.results.length - 1;
    var result = event.results[last][0].transcript;

    // display what is said on screen for testing purposes. -- Sander
    diagnostic.textContent = "I recieved: " + result + " with a confidence of: " + event.results[0][0].confidence;
    outputbig.textContent = result;
    console.log("Recieved something!");

    globalresult = result.toLowerCase();

    // For testing, the command 'start' can be used to start Jester. -- Sander
    if(result.toLowerCase() === "start" && trigger === false){
        trigger = true;
        startvalue = true;
        counter = 0;

    }
    // for testing, the command 'stop' can be used to stop jester. -- Sander
    else if(result.toLowerCase() === "stop"){
        startvalue = false;
        trigger = false;
        saythings().stop();
    }
    else{
        startvalue = false;
    }

    // When Jester is started, Jester will start with this, in this case, say Hallo to the user. -- Sander
    if(startvalue === true){
        saythings("Hallo");
        // After saying hello, we check to see if we get a result from the user. -- Sander
        checkIfResponse(result.toLowerCase());
    }

    // For testing purposes, we can say 'grap' to trigger Jester to say a joke. -- Sander
    if(result.toLowerCase() === "grap"){
        randomgrap();
    }

};

// Custom function that checks every second to see if the user has given a response. -- Sander
function checkIfResponse(){
    if(counter < 10 ) {
        setTimeout(function () {
            counter++;
            console.log(counter);
            // If no response is given while the function is running. -- Sander
            if(startvalue === true){
                console.log("No response");
            }
            // If the user has given a response while the function is running. -- Sander
            else{
                console.log("Response " + globalresult);
                counter = 9999;
                if(trigger == true){
                    response(globalresult);
                    // The response function is called, giving what the user has said as parameter. -- Sander
                }
                return 0;
            }
            // Run function again to keep on counting -- Sander
            checkIfResponse();
        }, 1000);
    }
    // If the user has not given a response within 10 seconds, run the Noresponse function. -- Sander
    else if(counter === 10){
        noresponse();
    }
    else{
    }
    return 0;
}


// This function is called whenever a user gives a response to Jester. -- Sander
function response(globalresult){
    // A random number is generated from 0 to 4 -- Sander
    var randnum = Math.floor(Math.random() * 4);

    // Select the random number that has been picked. -- Sander
    switch(randnum) {

        // wikipedia page - Picks a word from what you said to Jester and uses the Wikipedia API to search for that word and give you a description of that word. -- Davy
        case 0:
            Databaselog(user_key, "Wikipedia", globalresult, "");
            var str = globalresult;
            var amountwords = str.split(" ").length;
            var word = str.split(" ")[Math.floor(Math.random() * amountwords)];
            var keyword = word;
            $.getJSON('https://nl.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles='+ keyword +'&redirects=true&explaintext&exintro&origin=*',
                function(data) {
                    var page = data.query.pages;
                    console.log(page);
                    pageId = Object.keys(data.query.pages)[0];
                    var content = page[pageId].extract;
                    if(content != null){
                        console.log(content);

                        var sentence;

                        if(content.includes(".")){
                            sentence = content.split(".")[0];
                        }
                        else{
                            sentence = content.split(/(\r\n|\n|\r)/gm)[0];
                        }
                        console.log("Sentence: " + sentence);
                        saythings("Wat informatie over het woord " + word);
                        saythings(word + sentence);

                        // Should put this in a seperate function for cleaner code and ease of editing if needs be
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 13000);
                    }
                    else{
                        randomgrap();

                        // Should put this in a seperate function for cleaner code and ease of editing if needs be
                        counter = 0;
                        startvalue =  true;
                        setTimeout(function(){
                            checkIfResponse();
                        }, 13000);
                    }

                });
            console.log("Result in responsefunction: " + word);
            break;

        // ridicule the person - Creates a new random number and picks one of the possible scenario's to try and ridicule the person -- Sander
        case 1:
            Databaselog(user_key, "Belachelijk", globalresult, "");
            var newrandnum = Math.floor(Math.random() * 2);
            switch(newrandnum){
                // Jester asks if you're talking back to a toilet
                case 0:
                    saythings("Praat je nou terug tegen een WC?");
                    setTimeout(function(){
                        counter = 0;
                        startvalue = true;
                        checkIfResponse();
                    }, 5000);
                    break;
                // Jester tells the person that he/she is talking to a lifeless object
                case 1:
                    saythings("Je praat nu tegen een levensloos object, hoe kansloos is dat");
                    // Should put this in a seperate function for cleaner code and ease of editing if needs be
                    setTimeout(function(){
                        counter = 0;
                        startvalue = true;
                        checkIfResponse();
                    }, 5000);
                    break;
                default:
                    saythings("Wat een leuk shirt heb je aan");
                    saythings("Oh wacht, toch niet");
                    // Should put this in a seperate function for cleaner code and ease of editing if needs be
                    setTimeout(function(){
                        counter = 0;
                        startvalue = true;
                        checkIfResponse();
                    }, 5000);
            }
            break;

        // Life story - This is Jester's life's story, a long story about Jester. - Sander & Davy
        // Because of unknown reasons to me, saythings() does not like long sentences to be said. This is why we have formatted it like this for quick results. -- Sander
        // This piece of 'code' can be improved by putting the text in a text file for example and separating by enters. This would make the code cleaner and would improve ease of use. -- Sander
        case 2:
            Databaselog(user_key, "Levensverhaal", globalresult, "");
            saythings("Dit is mijn levensverhaal");

            saythings("Jester werkte als nar bij het koninklijk hof. Jester was niet alleen goed in het maken van grappen. Jester was slim genoeg om zaken te doen voor de koning. Jester gebruikte de koning als persoon in zijn moppen.");
            saythings("Op een dag maakte Jester zoals gewoonlijk grapjes over de koning, maar de koning voelde zich beledigd en riep: Bewakers, grijp Jester! Zet hem in de gevangenis, over drie dagen hangen we Jester op.");
            saythings("Drie dagen later werd Jester voor de rechter gedaagd. De koning vroeg aan Jester: Hoe wil je sterven?");
            saythings("Jester zegt hierop: Majesteit, ik kies ervoor om op hoge leeftijd te sterven. ");
            saythings("Ha….. ha….. ha……. ");
            saythings("Jester begon te lachen en de koning lachte met hem mee. De koning vergaf Jester. Jester bewees hiermee dat humor sterker is dan de sterkste man op aarde. ");
            saythings("Tegenwoordig neemt Jester mensen in de maling op de wc.");

            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                counter = 0;
                startvalue = true;
                checkIfResponse();
            }, 48000);
            break;

        // Repeat user - Whatever the person says, Jester will repeat. To improve how annoying Jester is, this should run in a loop for 3-4 times of repeating the user. -- Sander
        case 3:
            Databaselog(user_key, "Napraten", globalresult, "");
            saythings(globalresult);

            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                counter = 0;
                startvalue = true;
                checkIfResponse();
            }, 5000);
            break;
    }
}

// This function is called whenever the user does not give a response to Jester -- Sander
function noresponse(){
    // A random number is generated from 0 to 4
    var randnum = Math.floor(Math.random() * 4);
    counter = 0;

    // Select the random number that has been picked. -- Sander
    switch(randnum){

        // Jester asks why the user ignores her
        case 0:
            Databaselog(user_key, "Negeren", globalresult, "");
            saythings("Waarom negeer je mij?!");
            checkIfResponse();
            break;

        // Jester will start crying because she doesn't get a response
        case 1:
            // saythings("je maakt me aan het huilen");
            // var audio = new Audio('audio/huilen.mpeg');
            // audio.play();

            Databaselog(user_key, "Huilen", globalresult, "");
            saythings("je maakt me aan het huilen");
            setTimeout(function(){ var audio = new Audio('audio/huilen.mpeg');
                audio.play();
            }, 2800);

            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                checkIfResponse();
            }, 14000);
            break;

        // Jester tells a random Joke
        case 2:
            Databaselog(user_key, "Grap", globalresult, "");
            randomgrap();

            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                checkIfResponse();
            }, 13000);
            break;

        // Turn the light on and off -- Nigel
        case 3:
            Databaselog(user_key, "Licht", globalresult, "");
            saythings("Als er toch niemand is, zet ik het licht wel uit.");

            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 2800);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow";
                // var audio = new Audio('audio_file.mp3');
                // audio.play();
            }, 8000);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 8230);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow";
                // var audio = new Audio('audio_file.mp3');
                // audio.play();
            }, 8700);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 8880);
            setTimeout(function(){ document.body.style.backgroundColor = "yellow"; }, 9000);
            setTimeout(function(){ document.body.style.backgroundColor = "black"; }, 9180);
            setTimeout(function(){ document.body.style.backgroundColor = "white"; }, 12000);

            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                checkIfResponse();
            }, 11000);
            break;

        // Default throwback, this was made real quick as a response to have a couple more. This should be improved by putting it in a loop for cleaner code instead of just repeating saythings() -- Sander
        default:
            Databaselog(user_key, "Hey", globalresult, "");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");
            saythings("hey");


            // Should put this in a seperate function for cleaner code and ease of editing if needs be
            setTimeout(function(){
                checkIfResponse();
            }, 80000);
    }
    return 0;
}

// This sound is played whenever a joke has ended, to create more comedic effect -- Sander & Nigel
function endjokesound(){
    var randnum = Math.floor(Math.random() * 3);
    var audio;
    switch(randnum){
        case 0:
            audio = new Audio('audio/krekel.mpeg');
            break;
        case 1:
            audio = new Audio('audio/badumtss.mp3');
            break;
        case 2:
            audio = new Audio('audio/lachen.mpeg');
            break;
    }
    audio.play();
}

// This is the function for getting a random joke. The jokes are gotten from a JSON file that was created by Nigel & Davy.
function randomgrap(){
    var actual_JSON;
    loadJSON(function(response) {

        actual_JSON = JSON.parse(response);
        var randnum = Math.floor(Math.random() * actual_JSON.length + 1);

        if(!synth.speaking){
            saythings(actual_JSON[randnum]["buildup"]);
            setTimeout(function(){
                saythingswithend(actual_JSON[randnum]["punchline"], endjokesound);
            }, 1000);
        }
        // console.log(actual_JSON[randnum]["buildup"]);
        // console.log(actual_JSON[randnum]["punchline"]);
    });
}


// This loads the Jokes from the JSON file using AJAX
function loadJSON(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'grappen.json', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}


// If the user is done speaking, start the recognition again for continues recording -- Sander
recognition.onend = function() {
    recognition.start();
};

var synth = window.speechSynthesis;
var voices = [];

// Populates the list of voices that can be used by the Web speech -- Sander
function populateVoiceList() {
    voices = synth.getVoices();
    // console.log(synth.getVoices())
}


voices = synth.getVoices();

if (speechSynthesis.onvoiceschanged !== undefined) {
    speechSynthesis.onvoiceschanged = populateVoiceList;

}


// This is the main function that Jester uses for talking. whattosay is what Jester should say -- Sander
function saythings(whattosay){

    utterThis = new SpeechSynthesisUtterance(whattosay);
    // var selectedOption = "Google UK English Female";
    var selectedOption = "";
    for (i = 0; i < voices.length; i++) {
        if (voices[i].name === selectedOption) {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1;
    utterThis.rate = 1;
    synth.speak(utterThis);

    // console.log(utterThis.voice);
}

// This is a separate function to accommodate the functions that need something done after the speech has ended. Preferably you'd use method overloading for this, but Javascript does not have this...
function saythingswithend(whattosay, thefunction){

    utterThis = new SpeechSynthesisUtterance(whattosay);
    // var selectedOption = "Google UK English Female";
    var selectedOption = "";
    for (i = 0; i < voices.length; i++) {
        if (voices[i].name === selectedOption) {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1;
    utterThis.rate = 1;
    synth.speak(utterThis);

    utterThis.onend = function(event) {
        thefunction()
    }


    // console.log(utterThis.voice);
}

// console.log(synth.getVoices());

// This function is for Logging the responses that Jester gives and the things that the users say to the Database
function Databaselog(user_key, scenario, user_response, walkedinorout){

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
        }
    };
    // xmlhttp.open("GET", "gethint.php?q=" + str, true);
    xmlhttp.open("GET", "https://sandervanderburgt.com/speech-to-text/dbcall?user_key=" + user_key +"&scenario=" + scenario + "&user_response= " + user_response + "&walkedinorout=" + walkedinorout, true);
    xmlhttp.send();
}




// This piece of code is for using the Arduino -- Nigel
// var socket = io();
// socket.on('sensor', function(data) {
//
//     // data = data/20;
//     // if(data > 1){
//     //   data = 1;
//     // }
//
//     if(data > 90){
//         data = 90;
//     }
//
//     console.log(data);
//
//     document.getElementById("mijndivje").innerHTML = data + "  " + teller;
//
//     if (data < 75 && alGetrggrd == false && teller > 100) {
//         startvalue = true;
//         saythings("Hallo");
//         saythings("Hoe gaat het met je?");
//         checkIfResponse();
//         counter = 0;
//         teller = 0;
//         trigger = true;
//         alGetrggrd = true;
//         user_key = (Math.random().toString(36).substr(2, 16));
//         Databaselog(user_key, "", "", false);
//     } else if (data < 75 && alGetrggrd == true && teller > 600) {
//         startvalue = false;
//         alGetrggrd = false;
//         trigger = false;
//         teller = 0;
//         Databaselog(user_key, "", "", true);
//     } else {
//          teller++;
//
//     }
// });

function veranderAchtergrond(kleur) {
    document.body.style.backgroundColor = kleur;
}
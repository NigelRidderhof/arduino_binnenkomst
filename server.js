var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var five = require('johnny-five');
var board = new five.Board({
  port: "COM10"
});

board.on("ready", function() {
  var proximity = new five.Proximity({
    controller: "HCSR04",
    pin: 10
  });
	
	io.on('connection', function(socket){
		console.log('got socket connection');

		proximity.on("data", function() {
			console.log(this.cm);
		});

		  proximity.on("change", function() {
			  io.emit('sensor', this.cm);
		  });
		
		
		socket.on('disconnect', function(){
			console.log('lost socket connection...');
		});
	});
	
});


/* serves all the static files */
app.get(/^(.+)$/, function(req, res){ 
    console.log('static file request : ' + req.params);
    console.log('serving: ' +  __dirname + 'htmldocs' + req.params[0]);
    res.sendFile( __dirname + '/htmldocs' + req.params[0]); 
});

server.listen(3000);